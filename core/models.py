from django.db import models

import random

def generate_password(length=12):
    if length > 400:
        length = 400  # устанавливаем максимальную длину пароля
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+-=[]{}|;:,.<>?'
    password = ''.join(random.choice(chars) for i in range(length))
    return password