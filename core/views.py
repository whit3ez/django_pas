from django.http import HttpResponse
from django.shortcuts import render
from .models import generate_password
import random
def home(request):
    return render(request, 'password_generator.html')

def password_generator(request):
    length = request.GET.get('length', 12)
    length = int(length) # преобразуем в целое число
    password = generate_password(length)
    return render(request, 'password_generator.html', {'password': password})